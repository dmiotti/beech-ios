#!/usr/bin/env python

import gdata.spreadsheet.service
import getpass

supported_languages = ['fr', 'en', 'it', 'es', 'ru', 'da', 'nl', 'pl', 'fi', 'tr', 'pt']
spreadsheet_key = '0At3U671IYo82dDZjVlZvYWMwOGJuSzFycVViU0MwZkE'
worksheet_id = 'od6'

email = raw_input("Enter your gmail email: ")
password = getpass.getpass("Enter your gmail password: ")

gd_client = gdata.spreadsheet.service.SpreadsheetsService()
gd_client.email = email
gd_client.password = password
gd_client.ProgrammaticLogin()

feed = gd_client.GetListFeed(spreadsheet_key, worksheet_id)

dict = {}
for lang in supported_languages:
	dict[lang] = {}

def format_string(s):
	if not s:
		return None
	return unicode(s).encode('utf-8').replace("\"", "\\\"").replace("'=", "=").replace('\n', '\\n')

for row in feed.entry:
	raw = row.custom
    
	for key in raw:
		key = format_string(raw['key'].text)
		if not key:
			continue
        
		for lang in supported_languages:
			dict[lang][key] = format_string(raw[lang].text)

for lang in supported_languages:
	f = open("Beech/Resources/Strings/%s.lproj/Localizable.strings" % lang,'w')
	for key in dict[lang]:
		f.write('"%s" = "%s";\n' % (key, dict[lang][key]))
	f.close()

//
//  LoginViewController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "LoginViewController.h"
#import "BeechLoader.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLOR_BACKGROUND;
    
    BeechLoader *loader = [[BeechLoader alloc] init];
    loader.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|
                               UIViewAutoresizingFlexibleTopMargin   |
                               UIViewAutoresizingFlexibleLeftMargin  |
                               UIViewAutoresizingFlexibleRightMargin);
    loader.center = self.view.center;
    [self.view addSubview:loader];
    [loader startAnimating];
}

@end

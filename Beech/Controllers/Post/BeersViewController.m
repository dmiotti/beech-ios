//
//  BeersViewController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "BeersViewController.h"
#import "PostViewController.h"

@interface BeersViewController ()<UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic) NSArray *beers;
@property (nonatomic) UISearchBar *custSearchBar;
@property (nonatomic) UISearchDisplayController *custSearchDisplayController;
@property (nonatomic) NSTimer *downloadTimer;
@end

@implementation BeersViewController

- (id)init {
    self = [super init];
    if (self) {
        self.beers = @[];
        self.post = [Post object];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = L(@"Beers");
    
    id closeBtn = [[UIBarButtonItem alloc] initWithTitle:L(@"Cancel")
                                                   style:UIBarButtonItemStylePlain
                                                  target:self
                                                  action:@selector(closeBtnClicked:)];
    self.navigationItem.leftBarButtonItem = closeBtn;
    
    self.custSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 44.)];
    self.custSearchBar.barTintColor = COLOR_ORANGE;
    self.custSearchBar.tintColor = COLOR_BLACK_BAR;
    self.custSearchBar.delegate = self;
    self.custSearchBar.keyboardType = UIKeyboardAppearanceDark;
    self.custSearchBar.translucent = NO;
    self.custSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.custSearchBar
                                                                         contentsController:self];
    self.custSearchDisplayController.searchResultsDelegate = self;
    self.custSearchDisplayController.searchResultsDataSource = self;
    self.custSearchDisplayController.delegate = self;
    self.tableView.tableHeaderView = self.custSearchBar;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)closeBtnClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)refreshWithSearch:(NSString *)search {
    [[BeechAPI searchBeerWithString:search]
     continueWithBlock:^id(BFTask *task) {
         if (task.error) {
             [[[UIAlertView alloc] initWithTitle:task.error.localizedRecoverySuggestion
                                         message:task.error.localizedDescription
                                        delegate:nil
                               cancelButtonTitle:nil
                               otherButtonTitles:L(@"OK"), nil] show];
             return nil;
         }
         
         self.beers = task.result;
         UITableView *searchTable = self.searchDisplayController.searchResultsTableView;
         [searchTable reloadSections:[NSIndexSet indexSetWithIndex:0]
                    withRowAnimation:UITableViewRowAnimationFade];
         return nil;
     }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.custSearchBar becomeFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.beers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *beerCellIdentifier = @"beerCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:beerCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:beerCellIdentifier];
    }
    cell.textLabel.text = [self.beers[indexPath.row] name];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.post.beer = self.beers[indexPath.row];
    
    PostViewController *post = [[PostViewController alloc] init];
    post.post = self.post;
    [self.navigationController pushViewController:post animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] init];
    footer.backgroundColor = COLOR_BACKGROUND;
    return footer;
}

#pragma mark - UISearchBarDelegate

- (void)invalidateTimer {
    if (self.downloadTimer) {
        [self.downloadTimer invalidate];
        self.downloadTimer = nil;
    }
}

- (void)startTimer {
    if (self.downloadTimer) [self invalidateTimer];
    self.downloadTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                          target:self
                                                        selector:@selector(refreshWithSearchBarText)
                                                        userInfo:nil
                                                         repeats:NO];
}

- (void)refreshWithSearchBarText {
    [self refreshWithSearch:self.custSearchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self startTimer];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self startTimer];
    [self.custSearchDisplayController.searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView {
    self.custSearchBar.showsCancelButton = YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView {
    self.custSearchBar.showsCancelButton = NO;
}

#pragma mark - Keyboard notifications

- (void)moveTextViewForKeyboard:(NSNotification*)aNotification up:(BOOL)up {
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.custSearchDisplayController.searchResultsTableView.frame;
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    newFrame.size.height -= keyboardFrame.size.height * (up?1:-1);
    self.custSearchDisplayController.searchResultsTableView.frame = CGRectIntegral(newFrame);
    
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self moveTextViewForKeyboard:notification up:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveTextViewForKeyboard:notification up:NO];
}

@end

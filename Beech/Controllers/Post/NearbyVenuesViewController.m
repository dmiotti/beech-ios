//
//  NearbyVenuesViewController.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "NearbyVenuesViewController.h"
#import "Foursquare2.h"
#import "SVProgressHUD.h"
#import "LocationPickerView.h"
#import <MapKit/MapKit.h>

@interface NearbyVenuesViewController () <CLLocationManagerDelegate, LocationPickerViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) LocationPickerView *locationPickerView;
@property (nonatomic) FSVenue *selected;
@property (nonatomic) NSArray *nearbyVenues;
@end

@implementation NearbyVenuesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = L(@"NearbyVenues");
    
    self.view.backgroundColor = COLOR_BACKGROUND;
    
    id postBtn = [[UIBarButtonItem alloc] initWithTitle:L(@"Post")
                                                  style:UIBarButtonItemStyleDone
                                                 target:self
                                                 action:@selector(postBtnClicked:)];
    self.navigationItem.rightBarButtonItem = postBtn;
    
    self.locationPickerView = [[LocationPickerView alloc] initWithFrame:self.view.bounds];
    self.locationPickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.locationPickerView.tableViewDataSource = self;
    self.locationPickerView.tableViewDelegate = self;
    self.locationPickerView.shouldAutoCenterOnUserLocation = YES;
    self.locationPickerView.pullToExpandMapEnabled = NO;
    self.locationPickerView.defaultMapHeight = 220.0;           // larger than normal
    self.locationPickerView.parallaxScrollFactor = 0.7;         // little slower than normal.
    self.locationPickerView.backgroundViewColor = COLOR_BACKGROUND;
    
    // Optional setup
    self.locationPickerView.mapViewDidLoadBlock = ^(LocationPickerView *locationPicker) {
        locationPicker.mapView.mapType = MKMapTypeStandard;
        locationPicker.mapView.userTrackingMode = MKUserTrackingModeFollow;
    };
    self.locationPickerView.tableViewDidLoadBlock = ^(LocationPickerView *locationPicker) {
        locationPicker.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        locationPicker.tableView.showsVerticalScrollIndicator = NO;
        locationPicker.tableView.rowHeight = 55.;
        locationPicker.tableView.tintColor = COLOR_ORANGE;
    };
    [self.view addSubview:self.locationPickerView];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
    [self updateRightBarButtonStatus];
}

- (void)updateRightBarButtonStatus {
    self.navigationItem.rightBarButtonItem.enabled = self.selected != nil;
}

- (IBAction)postBtnClicked:(id)sender {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [[BeechAPI postAsync:self.post
                 atVenue:self.selected] continueWithBlock:^id(BFTask *task) {
        [SVProgressHUD dismiss];
        [self dismissViewControllerAnimated:YES completion:nil];
        return nil;
    }];
}

- (void)removeAllAnnotationExceptOfCurrentUser {
    MKMapView *mapView = self.locationPickerView.mapView;
    NSMutableArray *annForRemove = [[NSMutableArray alloc] initWithArray:mapView.annotations];
    if ([mapView.annotations.lastObject isKindOfClass:[MKUserLocation class]]) {
        [annForRemove removeObject:mapView.annotations.lastObject];
    } else {
        for (id <MKAnnotation> annot_ in mapView.annotations) {
            if ([annot_ isKindOfClass:[MKUserLocation class]] ) {
                [annForRemove removeObject:annot_];
                break;
            }
        }
    }
    
    [mapView removeAnnotations:annForRemove];
}

- (void)proccessAnnotations {
    [self removeAllAnnotationExceptOfCurrentUser];
    MKMapView *mapView = self.locationPickerView.mapView;
    [mapView addAnnotations:self.nearbyVenues];
}



- (void)getVenuesForLocation:(CLLocation *)location {
    [Foursquare2 venueSearchNearByLatitude:@(location.coordinate.latitude)
                                 longitude:@(location.coordinate.longitude)
                                     query:nil
                                     limit:nil
                                    intent:intentCheckin
                                    radius:@(500)
                                categoryId:nil
                                  callback:^(BOOL success, id result){
                                      if (success) {
                                          NSDictionary *dic = result;
                                          NSArray *venues = [dic valueForKeyPath:@"response.venues"];
                                          FSConverter *converter = [[FSConverter alloc] init];
                                          NSInteger before = self.nearbyVenues.count;
                                          self.nearbyVenues = [converter convertToObjects:venues];
                                          [self proccessAnnotations];
                                          UITableView *tableView = self.locationPickerView.tableView;
                                          tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                                          NSIndexSet *set = [NSIndexSet indexSetWithIndex:0];
                                          if (before == 0) {
                                              CGRect rect = self.locationPickerView.mapView.frame;
                                              rect.origin.y -= 150;
                                              self.locationPickerView.mapView.frame = rect;
                                              [tableView insertSections:set
                                                       withRowAnimation:UITableViewRowAnimationFade];
                                          } else [tableView reloadSections:set
                                                          withRowAnimation:UITableViewRowAnimationFade];
                                      }
                                  }];
}

- (void)setupMapForLocatoion:(CLLocation *)newLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.003;
    span.longitudeDelta = 0.003;
    CLLocationCoordinate2D location;
    location.latitude = newLocation.coordinate.latitude;
    location.longitude = newLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [self.locationPickerView.mapView setRegion:region animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];
    [self getVenuesForLocation:newLocation];
    [self setupMapForLocatoion:newLocation];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.nearbyVenues.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.nearbyVenues.count) {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font = FONT_MEDIUM_500(15.);
        cell.detailTextLabel.font = FONT_LIGHT_300(13.);
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    FSVenue *venue = self.nearbyVenues[indexPath.row];
    cell.textLabel.text = [venue name];
    cell.detailTextLabel.text = [venue detail];
    if (venue == self.selected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)userDidSelectVenue {
    [self updateRightBarButtonStatus];
    if (self.selected) {
        [self setupMapForLocatoion:[[CLLocation alloc] initWithLatitude:self.selected.location.coordinate.latitude
                                                              longitude:self.selected.location.coordinate.longitude]];
        [self.locationPickerView.mapView selectAnnotation:self.selected animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FSVenue *choosen = self.nearbyVenues[indexPath.row];
    NSMutableArray *idxToReload = [NSMutableArray array];
    if (self.selected == choosen) {
        self.selected = nil;
    }
    else {
        if (self.selected) {
            NSInteger idx = [self.nearbyVenues indexOfObject:self.selected];
            [idxToReload addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
        }
        self.selected = choosen;
    }
    [idxToReload addObject:indexPath];
    [tableView reloadRowsAtIndexPaths:idxToReload
                     withRowAnimation:UITableViewRowAnimationFade];
    [self userDidSelectVenue];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if (annotation == mapView.userLocation)
        return nil;
    
    static NSString *s = @"ann";
    MKAnnotationView *pin = [mapView dequeueReusableAnnotationViewWithIdentifier:s];
    if (!pin) {
        pin = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:s];
        pin.canShowCallout = YES;
        pin.image = ImageNamed(@"pin");
        pin.calloutOffset = CGPointMake(0, 0);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [button addTarget:self
                   action:@selector(checkinButton) forControlEvents:UIControlEventTouchUpInside];
        pin.rightCalloutAccessoryView = button;
        
    }
    return pin;
}

- (void)checkinButton {
    self.selected = self.locationPickerView.mapView.selectedAnnotations.lastObject;
    [self userDidSelectVenue];
}

@end

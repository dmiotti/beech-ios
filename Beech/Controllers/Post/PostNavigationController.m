//
//  PostNavigationController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "PostNavigationController.h"
#import "BeersViewController.h"

@implementation PostNavigationController

- (id)init {
    BeersViewController *beer = [[BeersViewController alloc] init];
    self = [super initWithRootViewController:beer];
    return self;
}

@end

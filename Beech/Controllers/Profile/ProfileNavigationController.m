//
//  ProfileNavigationController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "ProfileNavigationController.h"
#import "ProfileViewController.h"

@implementation ProfileNavigationController

- (id)init {
    ProfileViewController *post = [[ProfileViewController alloc] init];
    self = [super initWithRootViewController:post];
    return self;
}

@end

//
//  FeedNavigationController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "FeedNavigationController.h"
#import "FeedViewController.h"

@implementation FeedNavigationController

- (id)init {
    FeedViewController *feed = [[FeedViewController alloc] init];
    self = [super initWithRootViewController:feed];
    return self;
}

@end

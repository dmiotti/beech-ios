//
//  FeedViewController.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "FeedViewController.h"
#import "PostNavigationController.h"
#import "BeechLoader.h"
#import "PostCell.h"
#import "PostDetailViewController.h"

@interface FeedViewController ()
@property (nonatomic) NSArray *posts;
@property (nonatomic) BeechLoader *loading;
@end

@implementation FeedViewController

- (id)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.posts = @[];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = L(@"Feed");
    self.view.backgroundColor = COLOR_BACKGROUND;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(handleRefresh)
                  forControlEvents:UIControlEventValueChanged];
    
    id postBtn = [[UIBarButtonItem alloc] initWithImage:ImageNamed(@"tabbar-icon-post")
                                                  style:UIBarButtonItemStylePlain
                                                 target:self action:@selector(postBtnClicked:)];
    self.navigationItem.rightBarButtonItems = @[[UIBarButtonItem negativeSpacer], postBtn];
    
    self.loading = [[BeechLoader alloc] init];
    self.loading.center = CGPointMake(self.tableView.center.x, self.tableView.center.y-64.);
    self.loading.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|
                                     UIViewAutoresizingFlexibleTopMargin|
                                     UIViewAutoresizingFlexibleLeftMargin|
                                     UIViewAutoresizingFlexibleRightMargin);
    [self.tableView addSubview:self.loading];
    
    [self.loading startAnimating];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postDidCreate:)
                                                 name:NOTIFICATION_POST_CREATED
                                               object:nil];
}

- (IBAction)postBtnClicked:(id)sender {
    PostNavigationController *post = [[PostNavigationController alloc] init];
    [self presentViewController:post animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self handleRefresh];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UIRemoteNotificationType types = (UIRemoteNotificationTypeBadge|
                                      UIRemoteNotificationTypeAlert|
                                      UIRemoteNotificationTypeSound);
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:types];
}

#pragma mark - NSNotification

- (void)postDidCreate:(NSNotification *)notification {
    Post *post = notification.userInfo[@"post"];
    self.posts = [@[post] arrayByAddingObjectsFromArray:self.posts];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]
                          withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Refresh

- (void)handleRefresh {
    BFTask *task = [BeechAPI postsAsync];
    [task continueWithBlock:^id(BFTask *task) {
        
        [self.refreshControl endRefreshing];
        
        [UIView animateWithDuration:.35
                         animations:^{
                             [self.loading setAlpha:0.];
                         } completion:^(BOOL finished) {
                             if (finished) {
                                 [self.loading stopAnimating];
                             }
                         }];
        
        if (task.error) {
            [[[UIAlertView alloc] initWithTitle:[task.error localizedRecoverySuggestion]
                                        message:[task.error localizedDescription]
                                       delegate:nil
                              cancelButtonTitle:nil
                              otherButtonTitles:L(@"OK"), nil] show];
        }
        else {
            self.posts = task.result;
            [self.tableView reloadData];
        }
        
        return nil;
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.posts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PostCell heightForPost:self.posts[indexPath.row] forWidth:tableView.bounds.size.width];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *postCellIdentifier = @"postCellIdentifier";
    PostCell *cell = (PostCell *)[tableView dequeueReusableCellWithIdentifier:postCellIdentifier];
    
    if (!cell) {
        cell = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault
                               reuseIdentifier:postCellIdentifier];
    }
    
    cell.post = self.posts[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] init];
    footer.backgroundColor = COLOR_BACKGROUND;
    return footer;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PostDetailViewController *postDetail = [[PostDetailViewController alloc] init];
    postDetail.post = self.posts[indexPath.row];
    [self.navigationController pushViewController:postDetail animated:YES];
}

@end

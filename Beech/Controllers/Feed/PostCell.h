//
//  PostCell.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UITableViewCell
@property (nonatomic) Post *post;

+ (CGFloat)heightForPost:(Post *)post forWidth:(CGFloat)width;

@end

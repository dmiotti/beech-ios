//
//  PostCell.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "PostCell.h"

@interface PostCell ()
@property (nonatomic) UILabel *introLabel;
@property (nonatomic) UILabel *dateLabel;
@property (nonatomic) UILabel *quoteLabel;
@property (nonatomic) UIImageView *authorImageView;
@property (nonatomic) NSString *currentURLImageDisplayed;
@end

#define MARGIN 10.f
#define IMAGE_SIZE 48.f
#define PADD  3.

@implementation PostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.authorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(MARGIN, MARGIN, IMAGE_SIZE, IMAGE_SIZE)];
        self.authorImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        self.authorImageView.layer.cornerRadius = 3.;
        self.authorImageView.layer.masksToBounds = YES;
        self.authorImageView.backgroundColor = COLOR_BACKGROUND;
        [self.contentView addSubview:self.authorImageView];
        
        self.dateLabel = [[UILabel alloc] init];
        self.dateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:self.dateLabel];
        
        self.introLabel = [[UILabel alloc] init];
        self.introLabel.numberOfLines = 0;
        self.introLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:self.introLabel];
        
        self.quoteLabel = [[UILabel alloc] init];
        self.quoteLabel.numberOfLines = 0;
        self.quoteLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:self.quoteLabel];
    }
    return self;
}

- (void)setPost:(Post *)post {
    _post = post;
    
    self.introLabel.attributedText = [self.post introAttributedString];
    self.quoteLabel.attributedText = [self.post quoteAttributedString];
    self.dateLabel.attributedText = [self.post timePosted];
    
    NSString *imageURL = post.author.picture;
    if (imageURL) {
        self.authorImageView.hidden = NO;
        if (![self.currentURLImageDisplayed isEqualToString:imageURL]) {
            NSDate *startDate = [NSDate date];
            [self.authorImageView setBackgroundColor:COLOR_BACKGROUND];
            [self.authorImageView setImage:nil];
            [[BeechAPI getImageUrl:imageURL] continueWithBlock:^id(BFTask *task) {
                if (task.error || ![self.post.author.picture isEqual:imageURL])
                    return nil;
                
                _currentURLImageDisplayed = imageURL;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    UIImage *img = task.result;
                    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
                        [[UIScreen mainScreen] scale] == 2) {
                        img = [[UIImage alloc] initWithCGImage:img.CGImage
                                                         scale:2.
                                                   orientation:img.imageOrientation];
                    }
                    
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.authorImageView setBackgroundColor:[UIColor whiteColor]];
                        [self.authorImageView setImage:img];
                        if (fabs([startDate timeIntervalSinceNow]) > 1)
                            [self.authorImageView addAnimationType:kCATransitionFade];
                    });
                });
                return nil;
            }];
        }
    }
    else {
        self.authorImageView.hidden = YES;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offsetX = MARGIN + IMAGE_SIZE + MARGIN;
    CGFloat w = self.contentView.frame.size.width - offsetX - MARGIN;
    
    
    CGRect rect = [self.dateLabel.attributedText boundingRectWithSize:CGSizeMake(w, INFINITY)
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                              context:nil];
    self.dateLabel.frame = CGRectIntegral(CGRectMake(self.contentView.bounds.size.width-MARGIN-rect.size.width,
                                                     MARGIN, rect.size.width, rect.size.height));
    
    rect = [self.introLabel.attributedText boundingRectWithSize:CGSizeMake(w - self.dateLabel.frame.size.width - MARGIN,
                                                                           INFINITY)
                                                        options:(NSStringDrawingUsesLineFragmentOrigin|
                                                                 NSStringDrawingUsesFontLeading)
                                                        context:nil];
    self.introLabel.frame = CGRectIntegral(CGRectMake(offsetX, MARGIN, rect.size.width, rect.size.height));
    
    rect = [self.quoteLabel.attributedText boundingRectWithSize:CGSizeMake(w, INFINITY)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                        context:nil];
    self.quoteLabel.frame = CGRectIntegral(CGRectMake(offsetX, CGRectGetMaxY(self.introLabel.frame) + PADD, rect.size.width, rect.size.height));
}

+ (CGFloat)heightForPost:(Post *)post forWidth:(CGFloat)width {
    CGFloat height = MARGIN;
    height += IMAGE_SIZE;
    
    CGFloat temp = MARGIN;
    CGFloat offsetX = MARGIN + IMAGE_SIZE + MARGIN;
    CGFloat w = width - offsetX - MARGIN;
    
    CGRect rect = [[post introAttributedString] boundingRectWithSize:CGSizeMake(w - [[post timePosted] size].width - MARGIN, INFINITY)
                                                             options:(NSStringDrawingUsesLineFragmentOrigin|
                                                                      NSStringDrawingUsesFontLeading)
                                                             context:nil];
    temp += rect.size.height + PADD;
    
    rect = [[post quoteAttributedString] boundingRectWithSize:CGSizeMake(width, INFINITY)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                      context:nil];
    temp += rect.size.height;
    
    if (temp > height) height = temp;
    
    height += MARGIN;
    return height;
}

@end

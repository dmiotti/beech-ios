//
//  Constants.h
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#ifndef Beech_Constants_h
#define Beech_Constants_h

#define BREWERYDB_BASE          @"http://api.brewerydb.com/v2/"

#define COLOR_BACKGROUND    RGBA(246, 245, 240, 1)
#define COLOR_ORANGE        RGBA(248, 131,  71, 1)
#define COLOR_BLACK_BAR     RGBA( 51,  47,  41, 1)
#define COLOR_DATE_COLOR    RGBA(166, 159, 136, 1)

#define NOTIFICATION_POST_CREATED @"NOTIFICATION_POST_CREATED"

#define FONT_BOLD(font_size)        [UIFont boldSystemFontOfSize:font_size]
#define FONT_REGULAR_400(font_size) [UIFont systemFontOfSize:font_size]
#define FONT_MEDIUM_500(font_size)  [UIFont fontWithName:@"HelveticaNeue-Medium" size:font_size]
#define FONT_LIGHT_300(font_size)   [UIFont fontWithName:@"HelveticaNeue-Light" size:font_size]
#define FONT_THIN_200(font_size)    [UIFont fontWithName:@"HelveticaNeue-Thin" size:font_size]

// useful macros
#define L(s)                NSLocalizedString(s,s)
#define kDocumentsDirectory NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0]
#define IS_IPAD             UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define degreesToRadian(x)  (M_PI * (x) / 180.0)
#define Nillify(o)          (o == [NSNull null]) ? nil : o
#define IS_IPHONE_5         (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
#define ApplicationDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define ImageNamed(s)       [UIImage imageNamed:s]
#define RGBA(r,g,b,a)       [UIColor colorWithRed:(r / 255.) green:(g / 255.) blue:(b / 255.) alpha:a]
#define HEX(s)              [UIColor colorWithHexString:s]

#endif

//
//  AppDelegate.m
//  Beech

//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "AppDelegate.h"

#import <Parse/Parse.h>

#import "FeedNavigationController.h"
#import "ProfileNavigationController.h"
#import "Foursquare2.h"

@interface AppDelegate ()<UITabBarControllerDelegate>
@end

@implementation AppDelegate

- (void)customizeAppearence {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window.tintColor = [UIColor whiteColor];
    
    // the title in the navigation bar
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary:
                                               [[UINavigationBar appearance] titleTextAttributes]];
    titleBarAttributes[NSForegroundColorAttributeName] = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
    
    [[UINavigationBar appearance] setBarTintColor:COLOR_ORANGE];
    
    [[UITabBar appearance] setBackgroundColor:COLOR_BLACK_BAR];
    [[UITabBar appearance] setBarStyle:UIBarStyleBlack];
    [[UITabBar appearance] setTintColor:COLOR_ORANGE];
}

- (void)setupWindow {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self customizeAppearence];
    FeedNavigationController *feed = [[FeedNavigationController alloc] init];
    feed.tabBarItem = [[UITabBarItem alloc] initWithTitle:L(@"Feed")
                                                    image:ImageNamed(@"tabbar-icon-feed-default")
                                            selectedImage:ImageNamed(@"tabbar-icon-feed-active")];
    feed.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, 18.0);
    
    ProfileNavigationController *profile = [[ProfileNavigationController alloc] init];
    profile.tabBarItem = [[UITabBarItem alloc] initWithTitle:L(@"Profile")
                                                       image:ImageNamed(@"tabbar-icon-profile-default")
                                               selectedImage:ImageNamed(@"tabbar-icon-profile-active")];
    profile.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, 18.0);
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = @[feed, profile];
    self.tabBarController.delegate = self;
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
}

- (void)setupThirdparties {
    [Beer registerSubclass];
    [User registerSubclass];
    [Post registerSubclass];
    [Parse setApplicationId:PARSE_APPID clientKey:PARSE_CLIENT_KEY];
    [PFFacebookUtils initializeFacebook];
    
    [Foursquare2 setupFoursquareWithClientId:FSQUARE_CLIENT_KEY
                                      secret:FSQUARE_CLIENT_SECRET
                                 callbackURL:@"beech://foursquare"];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self setupThirdparties];
    [self setupWindow];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSString *fbId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
    [FBSettings setDefaultAppID:fbId];
    [FBAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    if ([FBAppCall handleOpenURL:url
               sourceApplication:sourceApplication
                     withSession:[PFFacebookUtils session]])
        return YES;
    
    if ([Foursquare2 handleURL:url])
        return YES;
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveEventually];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

@end

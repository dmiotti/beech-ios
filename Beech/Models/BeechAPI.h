//
//  Beech.h
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Post.h"
#import "User.h"
#import "Beer.h"
#import "FSVenue.h"
#import "FSConverter.h"

@interface BeechAPI : NSObject

+ (BFTask *)loginAsync;
+ (BFTask *)postsAsync;
+ (BFTask *)postAsync:(Post *)post atVenue:(FSVenue *)venue;
+ (BFTask *)searchBeerWithString:(NSString *)search;
+ (BFTask *)getImage:(PFFile *)image bundled:(BOOL)bundled;
+ (BFTask *)getImageUrl:(NSString *)url;

@end

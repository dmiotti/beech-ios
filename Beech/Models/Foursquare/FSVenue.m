//
//  FSVenue.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "FSVenue.h"

@implementation FSLocation

@end

@implementation FSVenue

- (id)init {
    self = [super init];
    if (self) {
        self.location = [[FSLocation alloc] init];
    }
    return self;
}

- (CLLocationCoordinate2D)coordinate {
    return self.location.coordinate;
}

- (NSString *)title {
    return self.name;
}

- (NSString *)detail {
    if (self.location.address)
        return [NSString stringWithFormat:@"%@m, %@",
                self.location.distance,
                self.location.address];

    return [NSString stringWithFormat:@"%@m",
            self.location.distance];
}

@end

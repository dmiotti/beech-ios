//
//  FSConverter.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSConverter : NSObject

- (NSArray *)convertToObjects:(NSArray *)venues;

@end

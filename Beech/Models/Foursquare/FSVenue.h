//
//  FSVenue.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface FSLocation : NSObject {
    CLLocationCoordinate2D _coordinate;
}

@property CLLocationCoordinate2D coordinate;
@property (nonatomic) NSNumber *distance;
@property (nonatomic) NSString *address;

@end

@interface FSVenue : NSObject<MKAnnotation>

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *venueId;
@property (nonatomic) FSLocation *location;
@property (readonly) NSString *detail;

@end

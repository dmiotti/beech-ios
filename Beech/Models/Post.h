//
//  Post.h
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Parse/Parse.h>

@class User;
@class Beer;

@interface Post : PFObject<PFSubclassing>

@property NSString *content;
@property NSNumber *thumbsUp;
@property NSNumber *thumbsDown;
@property NSMutableArray *messages;
@property User *author;
@property PFGeoPoint *geoPoint;
@property Beer *beer;
@property NSString *address;
@property NSString *placeName;
@property NSString *fsId;

- (NSMutableAttributedString *)quoteAttributedString;
- (NSMutableAttributedString *)introAttributedString;

@end

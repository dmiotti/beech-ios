//
//  Beech.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "BeechAPI.h"
#import "LoginViewController.h"
#import "AFNetworking.h"
#import "TMCache.h"

#import <MapKit/MapKit.h>

@implementation BeechAPI

+ (BFTask *)loginAsync {
    User *user = [User currentUser];
    if (user)
        return [BFTask taskWithResult:user];
    
    LoginViewController *login = [[LoginViewController alloc] init];
    login.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UIViewController *ctrl = ApplicationDelegate.tabBarController.selectedViewController;
    [ctrl presentViewController:login
                       animated:YES
                     completion:nil];
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [PFFacebookUtils logInWithPermissions:@[@"email", @"user_about_me"]
                                    block:^(PFUser *user, NSError *error) {
                                        [login dismissViewControllerAnimated:YES
                                                                  completion:nil];
                                        
                                        if (error) {
                                            [task setError:error];
                                        }
                                        else {
                                            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                                            currentInstallation[@"user"] = user;
                                            [currentInstallation saveEventually];
                                            
                                            [task setResult:user];
                                        }
                                    }];
    return task.task;
}

+ (BFTask *)postsAsync {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [[self loginAsync] continueWithBlock:^id(BFTask *loginTask) {
        PFQuery *query = [Post query];
        [query orderByDescending:@"updatedAt"];
        [query includeKey:@"beer"];
        [query includeKey:@"author"];
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                [task setError:error];
            }
            else {
                [task setResult:objects];
            }
        }];
        return nil;
    }];
    return task.task;
}

+ (BFTask *)postAsync:(Post *)post atVenue:(FSVenue *)venue {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [[self loginAsync] continueWithBlock:^id(BFTask *loginTask) {
        if (loginTask.error) {
            [task setError:loginTask.error];
            return nil;
        }
        
        post.author = loginTask.result;
        CLLocationCoordinate2D coordinate = venue.location.coordinate;
        post.geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude
                                               longitude:coordinate.longitude];
        post.address = venue.location.address;
        post.placeName = venue.name;
        post.fsId = venue.venueId;
        [post saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                [task setError:error];
                return;
            }
            [task setResult:post];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_POST_CREATED
                                                                object:nil
                                                              userInfo:@{@"post": post}];
        }];
        return nil;
    }];
    return task.task;
}

+ (BFTask *)searchBeerWithString:(NSString *)search {
    if (!search || [search length] == 0)
        return [BFTask taskWithResult:@[]];
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    [[self loginAsync] continueWithBlock:^id(BFTask *loginTask) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"key"] = BREWERYDB_API_KEY;
        params[@"q"] = search;
        [manager GET:[NSString stringWithFormat:@"%@/search", BREWERYDB_BASE]
          parameters:params
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSArray *beers = [responseObject[@"data"] mapObjectsUsingBlock:^id(id obj, NSUInteger idx) {
                     return [Beer beerWithDictionary:obj];
                 }];
                 [task setResult:beers];
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [task setError:error];
             }];
        return nil;
    }];
    return task.task;
}


+ (BFTask *)getImageUrl:(NSString *)url {
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    
    void (^handleResponse)(UIImage *img, NSError *err) = ^(UIImage *img, NSError *err){
        if (![NSThread isMainThread]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (err) {
                    [task setError:err];
                }
                else {
                    [task setResult:img];
                }
            });
        }
        else {
            if (err) {
                [task setError:err];
            }
            else {
                [task setResult:img];
            }
        }
    };
    
    [[TMCache sharedCache] objectForKey:url
                                  block:^(TMCache *cache, NSString *key, id object) {
                                      if (object) {
                                          handleResponse(object, nil);
                                      } else {
                                          NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                                                                 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                                                             timeoutInterval:60.];
                                          
                                          AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                                          [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              UIImage *image = [UIImage imageWithData:responseObject];
                                              [[TMCache sharedCache] setObject:image
                                                                        forKey:key
                                                                         block:^(TMCache *cache, NSString *key, id object) {
                                                                             handleResponse(image, nil);
                                                                         }];
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              handleResponse(nil, error);
                                          }];
                                          [operation start];
                                      }
                                  }];
    return task.task;
}

+ (BFTask *)getImage:(PFFile *)image bundled:(BOOL)bundled {
    if (!image) return [BFTask taskWithResult:nil];
    
    BFTaskCompletionSource *task = [BFTaskCompletionSource taskCompletionSource];
    if (bundled) {
        // check file on bundle
        NSURL *imageURL = [NSURL URLWithString:image.url];
        NSString *filename = [[imageURL lastPathComponent] stringByDeletingPathExtension];
        NSRange range = [filename rangeOfString:@"-" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSInteger start = range.location+range.length;
            NSString *bundleFilename = [filename substringWithRange:NSMakeRange(start, [filename length] - start)];
            UIImage *bundleImage = ImageNamed(bundleFilename);
            if (bundleImage) {
                return [BFTask taskWithResult:bundleImage];
            }
        }
    }
    
    [[BeechAPI getImageUrl:image.url] continueWithBlock:^id(BFTask *loginTask) {
        if (loginTask.result) {
            [task setResult:loginTask.result];
        }
        else {
            [task setError:loginTask.error];
        }
        return nil;
    }];
    
    return task.task;
}

@end

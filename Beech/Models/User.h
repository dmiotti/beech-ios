//
//  User.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Parse/Parse.h>

@interface User : PFUser<PFSubclassing>
@property NSString *picture;
@end

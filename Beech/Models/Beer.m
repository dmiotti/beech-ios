//
//  Beer.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "Beer.h"
#import <Parse/PFObject+Subclass.h>

@implementation Beer

@dynamic name;
@dynamic breweryId;

+ (NSString *)parseClassName {
    return @"Beer";
}

+ (Beer *)beerWithDictionary:(NSDictionary *)dictionary {
    Beer *beer = [Beer object];
    beer.name = Nillify(dictionary[@"name"]);
    beer.breweryId = Nillify(dictionary[@"id"]);
    return beer;
}

@end

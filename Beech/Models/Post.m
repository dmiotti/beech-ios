//
//  Post.m
//  Beech
//
//  Created by David Miotti on 08/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "Post.h"
#import <Parse/PFObject+Subclass.h>

@implementation Post

@dynamic content;
@dynamic thumbsUp;
@dynamic thumbsDown;
@dynamic messages;
@dynamic author;
@dynamic geoPoint;
@dynamic beer;
@dynamic address;
@dynamic placeName;
@dynamic fsId;

+ (NSString *)parseClassName {
    return @"Post";
}


#pragma mark - Generate string displayed

- (NSMutableAttributedString *)quoteAttributedString {
    NSMutableString *full = [NSMutableString stringWithFormat:L(@"DidDrink"),
                             [self.beer.name capitalizedString]];
    if (self.content && [self.content length] > 0) {
        [full appendString:@" "];
        [full appendFormat:L(@"PostQuote"), self.content];
    }
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:full];
    [attr addAttribute:NSFontAttributeName
                 value:FONT_REGULAR_400(14.)
                 range:NSMakeRange(0, [attr.string length])];
    [attr addAttribute:NSForegroundColorAttributeName
                 value:COLOR_BLACK_BAR
                 range:NSMakeRange(0, [attr.string length])];
    [attr addAttribute:NSFontAttributeName
                 value:FONT_MEDIUM_500(14.)
                 range:[attr.string rangeOfString:[self.beer.name capitalizedString]]];
    return attr;
}


- (NSMutableAttributedString *)introAttributedString {
    NSString *placeText = [NSString stringWithFormat:L(@"at"), self.placeName];
    NSString *introText = [NSString stringWithFormat:@"%@ %@", self.author.username, placeText];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:introText];
    [attr addAttribute:NSFontAttributeName
                 value:FONT_MEDIUM_500(14.)
                 range:NSMakeRange(0, [attr.string length])];
    [attr addAttribute:NSForegroundColorAttributeName
                 value:COLOR_BLACK_BAR
                 range:NSMakeRange(0, [attr.string length])];
    [attr addAttribute:NSFontAttributeName
                 value:FONT_REGULAR_400(12.)
                 range:[attr.string rangeOfString:placeText]];
    [attr addAttribute:NSForegroundColorAttributeName
                 value:COLOR_DATE_COLOR
                 range:[attr.string rangeOfString:placeText]];
    return attr;
}

@end

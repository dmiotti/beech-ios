//
//  Beer.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Parse/Parse.h>

@interface Beer : PFObject<PFSubclassing>

@property NSString *name;
@property NSString *breweryId;

+ (Beer *)beerWithDictionary:(NSDictionary *)dictionary;

@end

//
//  NSArray+Map.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Map)

- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block;

@end

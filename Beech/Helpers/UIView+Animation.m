//
//  UIView+Animation.m
//  DPK
//
//  Created by David Miotti on 4/14/13.
//  Copyright (c) 2013 Wopata. All rights reserved.
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

- (void)addAnimationType:(NSString *)type {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = type;
    [self.layer addAnimation:transition forKey:nil];
}

- (void)addAnimationType:(NSString *)type forDuration:(NSTimeInterval)duration {
    CATransition *transition = [CATransition animation];
    transition.duration = duration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = type;
    [self.layer addAnimation:transition forKey:nil];
}

@end

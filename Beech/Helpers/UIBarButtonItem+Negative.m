//
//  UIBarButtonItem+Negative.m
//  Boddy
//
//  Created by David Miotti on 28/01/14.
//  Copyright (c) 2014 Wopata. All rights reserved.
//

#import "UIBarButtonItem+Negative.h"

@implementation UIBarButtonItem (Negative)

+ (UIBarButtonItem *)negativeSpacer {
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                    target:nil
                                                                                    action:nil];
    negativeSpacer.width = -10.0f;
    return negativeSpacer;
}

@end

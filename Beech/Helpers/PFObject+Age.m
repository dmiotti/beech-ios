//
//  PFObject+Age.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "PFObject+Age.h"

@implementation PFObject (Age)

- (NSAttributedString *)timePosted {
    NSTimeInterval age = fabsf([self.createdAt timeIntervalSinceNow]);
    
    NSDictionary *formats = @{NSFontAttributeName: FONT_REGULAR_400(12.),
                              NSForegroundColorAttributeName: COLOR_DATE_COLOR};
    if (age < 60) {
        return [[NSAttributedString alloc] initWithString:@"< 1min" attributes:formats];
    }
    if (age < 3600) {
        int val = round(age/60);
        if (val == 1) return [[NSAttributedString alloc] initWithString:@"1min" attributes:formats];
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dmin", val]
                                               attributes:formats];
    }
    if (age < 3600 * 24) {
        int val = round(age/3600);
        if (val == 1) return [[NSAttributedString alloc] initWithString:@"1h" attributes:formats];
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dh", val]
                                               attributes:formats];
    }
    if (age < 3600 * 24 * 30.5) {
        int val = round(age/(3600*24));
        if (val == 1) return [[NSAttributedString alloc] initWithString:@"1j" attributes:formats];
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dj", val]
                                               attributes:formats];
    }
    if (age < 3600 * 24 * 30.5 * 12) {
        int val = round(age/(3600*24*30.5));
        if (val == 1) return [[NSAttributedString alloc] initWithString:@"1m" attributes:formats];
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dm", val]
                                               attributes:formats];
    }
    
    return [[NSAttributedString alloc] initWithString:@">1y" attributes:formats];
}

@end

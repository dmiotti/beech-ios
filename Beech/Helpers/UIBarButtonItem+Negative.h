//
//  UIBarButtonItem+Negative.h
//  Boddy
//
//  Created by David Miotti on 28/01/14.
//  Copyright (c) 2014 Wopata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Negative)

+ (UIBarButtonItem *)negativeSpacer;

@end

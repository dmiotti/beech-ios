//
//  BeechLoader.h
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechLoader : UIView

- (void)startAnimating;
- (void)stopAnimating;

@property (readonly) BOOL isAnimating;

@end

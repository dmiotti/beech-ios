//
//  BeechLoader.m
//  Beech
//
//  Created by David Miotti on 09/02/14.
//  Copyright (c) 2014 David Miotti. All rights reserved.
//

#import "BeechLoader.h"

@interface BeechLoader ()
@property (nonatomic) UIImageView *imageView;
@end

@implementation BeechLoader

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, 70., 70.)];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        NSMutableArray *images = [NSMutableArray array];
        for (int i = 0; i < 29; i++)
            [images addObject:ImageNamed(([NSString stringWithFormat:@"tmp-%d", i]))];
        self.imageView.animationImages = images;
        self.imageView.animationDuration = 2.5;
        self.imageView.autoresizingMask = (UIViewAutoresizingFlexibleHeight|
                                           UIViewAutoresizingFlexibleWidth);
        self.imageView.alpha = 0;
        [self addSubview:self.imageView];
    }
    return self;
}

- (BOOL)isAnimating {
    return [self.imageView isAnimating];
}

- (void)startAnimating {
    if (self.imageView.alpha >= 1.) return;
    
    [UIView animateWithDuration:.35
                     animations:^{ self.imageView.alpha = 1.; }];
    [self.imageView startAnimating];
}

- (void)stopAnimating {
    if (self.imageView.alpha <= 0.) return;
    [UIView animateWithDuration:.35
                     animations:^{ self.imageView.alpha = 0.; }];
    [self.imageView stopAnimating];
}

@end

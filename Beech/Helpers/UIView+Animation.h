//
//  UIView+Animation.h
//  DPK
//
//  Created by David Miotti on 4/14/13.
//  Copyright (c) 2013 Wopata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>

@interface UIView (Animation)

- (void)addAnimationType:(NSString *)type;
- (void)addAnimationType:(NSString *)type forDuration:(NSTimeInterval)duration;

@end
